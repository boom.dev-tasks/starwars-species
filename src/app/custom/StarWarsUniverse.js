import Species from './Species';
var EventEmitter = require('eventemitter3');


export default class StarWarsUniverse extends EventEmitter {
    constructor(maxSpecies) {
        super();
        this.species = [];
        this._maxSpecies = maxSpecies;
    }

    static get events() {
        const obj = {
            MAX_SPECIES_REACHED: 'max_species_reached',
            SPECIES_CREATED: 'species_created'
        }
        return obj;
    }

    get speciesCount() {
        return this.species.length;
    }

    createSpecies() {
        const species = new Species();
        species.on(StarWarsUniverse.events.SPECIES_CREATED, () => { this._onSpeciesCreated(species) }); // listener

        species.init(`https://swapi.boom.dev/api/species/${this.speciesCount + 1}`);
    }

    _onSpeciesCreated(species) { // should it recive object? should it listen for SPECIES_CREATED?
        this.species.push(species)

        this.emit(StarWarsUniverse.events.SPECIES_CREATED, { speciesCount: this.speciesCount });

        if (this.speciesCount === this._maxSpecies) {
            this.emit(StarWarsUniverse.events.MAX_SPECIES_REACHED);
        }
        else {
            this.createSpecies();
        }
    }
}