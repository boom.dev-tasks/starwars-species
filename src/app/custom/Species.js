var EventEmitter = require('eventemitter3');

export default class Species extends EventEmitter {
    constructor() {
        super();
        this.name = null,
            this.classification = null
    }

    static get events() {
        const obj = { SPECIES_CREATED: 'species_created' }
        return obj;
    }

    async init(URL) {
        const { name, classification } = await fetch(URL).then(r => r.json())
        this.name = name;
        this.classification = classification;

        this.emit(Species.events.SPECIES_CREATED); // event trigger
    }
}